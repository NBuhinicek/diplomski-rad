Korišteni paketi programskog jezika Python
--------------------------------------------------------------------------------------------------------------
Za implementacije generatora korišten je programski jezik **Python, verzija 3.7.2**. 

Dodatno, korišteni su sljedeći paketi programskog jezika Python:

- **Osnovni paketi**
- os
- time
- datetime
- statistics
- **Dodatni paketi**
- scipy (verzija 1.3.1)
- 	numpy (verzija 1.19.5)
- 	pandas (verzija 1.2.4)
- 	seaborn (verzija 0.11.1)
- 	sklearn (verzija 0.0) 
- 	matplotlib (verzija 3.3.2)
- 	ydata_synthetic (verzija 0.3.0)

Implementacije generatora su pisane u razvojnom okviru **jupyter notebook, verzija 4.4.0**.


Pokretanje implementiranih generatora
----------------------------------------------------------------------------------------------------------------
U sklopu praktičnog dijela napisana su četiri generatora i program koji priprema korišten stvarni skup podataka u oblik pogodan za rad modela prostorno-vremenske regresije i generatora 2., 3. i 4. Kodove i pripadni stvarni skup podataka moguće je preuzeti na poveznici https://gitlab.com/NBuhinicek/diplomski-rad. 

Svaki program napisan je kao zasebna jupyter bilježnica. Razvijanje rješenja u jupyter bilježnicama se svodi na pisanje koda i organizaciju kroz smislene slijedne cjeline koje se tada redom pokreću.

Kodovi generatora se temelje na prethodno objašnjenim algoritmima (poglavlje 3.). Na početku svake bilježnice je blok koji učitava pakete potrebne za rad. Nadalje slijedi blok s podesivim varijablama za rad generatora (spomenuti kroz poglavlje 3.). Tada se izvršava glavni dio bilježnice, tj. generiranje umjetnog skupa podataka, a nakon toga slijede analize generiranih podataka. Glavni dijelovi koda su u bilježnicama popraćeni komentarima.
